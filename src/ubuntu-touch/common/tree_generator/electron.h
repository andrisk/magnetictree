/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#undef USE_GLM

#ifndef ELECTRON_H
#define ELECTRON_H


class electron
{
public:

  float x;     //position (only 2D used)
  float y;
  float v[3];  //speed
  float m;     //mass
  float q;     //charge

  float t;     //current time
  float maxT;  //electron lifespan

  float alpha; //rate at which the electron charge change

  electron(float pos_x, float pos_y, float velocity[3], float mass, float chargeQ, float lifespan, float alphaQ);
  void step(float Fb, float dt);



  float newQ();

  private:
  void stepNoGlm(float Fb, float dt);
#ifdef USE_GLM
  void stepGlm(float Fb, float dt);
#endif

};

#endif // ELECTRON_H
