/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 - 2020 Andrej Trnkoci <andrej.trnkoci@gmail.com>        *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "treegeneratorinterface.h"
#include <QSvgRenderer>
#include <QImage>
#include <QPainter>
#include <math.h>

TreeGeneratorInterface::TreeGeneratorInterface()
{
  Rules = new TreeGeneratingRules(Qt::black, Qt::white);
  Rules->parameters.startX = 300;
  Rules->branchesThisLvl= 9;
  Trees.push_back(*(new magneticTree()));  //TBD check if it is Ok
  Trees[0].Generate(*Rules, NULL);
  Trees[0].TracePaths();
  lastGeneratedSvgString = "";
  lastGeneratedSvgTempFile = "";
}

TreeGeneratorInterface::~TreeGeneratorInterface()
{
    for (int x = 0; x < toBeDeletedList.size(); x++)
    {
        QFile::remove(toBeDeletedList[x]);
    }
}

QString TreeGeneratorInterface::generateSvgImage()
{
  //rules->levelColors.push_back(Qt::black);
  //rules->levelColors.push_back(Qt::black);
  //rules->levelColors.push_back(Qt::black);
  Trees[0].ClearRenderData();
  Trees[0].Generate(*Rules, NULL);
  Trees[0].TracePaths();
  lastGeneratedSvgString =  Trees[0].GenerateSvg();
  lastGeneratedSvgTempFile = "";
  return lastGeneratedSvgString.toUtf8().toBase64();
}

void TreeGeneratorInterface::setBranchLevelsNumber(uint levels)
{
    Rules->branchLevels = levels;
}

uint TreeGeneratorInterface::getBranchLevelsNumber()
{
    return Rules->branchLevels;
}

void TreeGeneratorInterface::setFirstLevelBranchesNumber(uint branches)
{
    Rules->branchesThisLvl = branches;
}

void TreeGeneratorInterface::setNextLevelBranchesDecrement(int decrement)
{
    Rules->dBrnchesNextLvl = decrement;
}

void TreeGeneratorInterface::setQmultiplier(float qMultiplier)
{
    Rules->qMultiplier = qMultiplier;
}

void TreeGeneratorInterface::setAlpha(float alpha)
{
    Rules->parameters.alpha = alpha;
}

void TreeGeneratorInterface::setAlphaMultiplier(float alphaMultiplier)
{
  Rules->alphaMultiplier = alphaMultiplier;
}


void TreeGeneratorInterface::setRootLength(uint rootLength)
{
    Rules->parameters.T = rootLength;
}

void TreeGeneratorInterface::setLengthMultiplier(float lengthMultiplier)
{
    Rules->tMultiplier = lengthMultiplier;
}

void TreeGeneratorInterface::setStartSize(uint startSize)
{
    Rules->parameters.size = startSize;
}

void TreeGeneratorInterface::setDSize(float dSize)
{
    Rules->parameters.dSize = dSize;
}

void TreeGeneratorInterface::setSizeMultiplier(float sizeMultiplier)
{
    Rules->sizeMultiplier = sizeMultiplier;
}

QString TreeGeneratorInterface::getLastGeneratedSvg()
{
    return lastGeneratedSvgString;
}

void TreeGeneratorInterface::saveImageFile(QString fileName, bool exportToPng)
{
    if (exportToPng)
    {
       exportAsPng(fileName, 1080, 1920); 
    }
    else
    {
        QUrl fileUrl(fileName);
        QFile newSvgFile(fileUrl.path());
        if (newSvgFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
           QTextStream outStream(&newSvgFile);
           outStream << lastGeneratedSvgString;
        }
        newSvgFile.close();
    }
}

void TreeGeneratorInterface::exportAsPng(QString outputFileName, int width, int height)
{
    QByteArray* svgImageInByteArray = new QByteArray();
    svgImageInByteArray->append(lastGeneratedSvgString);
    QSvgRenderer renderer(*svgImageInByteArray);
    int svgWidth = Trees[0].maxX - Trees[0].minX;
    int svgHeight = Trees[0].maxY - Trees[0].minY;
    float aspectRatio = ((float)svgHeight) / svgWidth;
    QImage *image = new QImage(1080, std::trunc(1080 * aspectRatio), QImage::Format_ARGB32);
    QPainter *painter = new QPainter(image);
    renderer.render(painter);
    image->save(outputFileName);
    delete painter;
    delete image;
    delete svgImageInByteArray;   
}

void TreeGeneratorInterface::setTreeColor(QColor treeColor)
{
    Rules->levelColors.clear();
    Rules->levelColors.push_back(treeColor);
    Rules->parameters.color = treeColor;
}

void TreeGeneratorInterface::setBackgroundColor(QColor backgroundColor)
{
    Rules->backgroundColor = backgroundColor;
}

QColor TreeGeneratorInterface::getBackgroundColor()
{
    return Rules->backgroundColor;
}

void TreeGeneratorInterface::createNewTempFile(QString fileName, bool exportToPng)
{
    toBeDeletedList.push_back(fileName);
    saveImageFile(fileName, exportToPng);
    lastGeneratedSvgTempFile = fileName;
    return;
}

QString TreeGeneratorInterface::saveImageFileAsTemporary(QString fileName, bool exportToPng)
{
    if (lastGeneratedSvgTempFile == "" || toBeDeletedList.size() == 0)
    {
        createNewTempFile(fileName, exportToPng);
        return fileName;
    }
    else
    {
        QString existingFilePath = toBeDeletedList.back();
        if (QFile::rename(existingFilePath, fileName))
        {
            toBeDeletedList.pop_back();
            toBeDeletedList.push_back(fileName);
            lastGeneratedSvgTempFile = fileName;
            saveImageFile(fileName, exportToPng);
        }
        else
        {
            createNewTempFile(fileName, exportToPng);
        }
        return fileName;
    }
}




