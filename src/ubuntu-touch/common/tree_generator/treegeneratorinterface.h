/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 - 2020 Andrej Trnkoci <andrej.trnkoci@gmail.com>        *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef TREEGENERATORINTERFACE_H
#define TREEGENERATORINTERFACE_H

#include <QObject>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QUrl>
#include <QColor>

#include <vector>
#include "magnetictree.h"

class TreeGeneratorInterface : public QObject
{
  Q_OBJECT

  //Q_PROPERTY(QString generateSvgImage READ generateSvgImage)
  Q_PROPERTY(uint branchLevels READ getBranchLevelsNumber WRITE setBranchLevelsNumber)
  Q_PROPERTY(uint firstLevelBranchesNumber WRITE setFirstLevelBranchesNumber)
  Q_PROPERTY(int nextLevelBranchesDecrement WRITE setNextLevelBranchesDecrement)
  Q_PROPERTY(float qMultiplier WRITE setQmultiplier)
  Q_PROPERTY(float alphaMultiplier WRITE setAlphaMultiplier)
  Q_PROPERTY(float alpha WRITE setAlpha)
  Q_PROPERTY(uint rootLength WRITE setRootLength)
  Q_PROPERTY(float lengthMultiplier WRITE setLengthMultiplier)
  Q_PROPERTY(uint startSize WRITE setStartSize)
  Q_PROPERTY(float dSize WRITE setDSize)
  Q_PROPERTY(float sizeMultiplier WRITE setSizeMultiplier)
  Q_PROPERTY(QString lastSvgString READ getLastGeneratedSvg)
  Q_PROPERTY(QColor treeColor WRITE setTreeColor)
  Q_PROPERTY(QColor backgroundColor WRITE setBackgroundColor READ getBackgroundColor)

public:
  Q_INVOKABLE void saveImageFile(QString fileName, bool exportToPng);
  Q_INVOKABLE QString saveImageFileAsTemporary(QString fileName, bool exportToPng);
  Q_INVOKABLE QString generateSvgImage();
  std::vector<magneticTree> Trees;
  TreeGeneratorInterface();
  ~TreeGeneratorInterface();
  void setBranchLevelsNumber(uint levels);
  uint getBranchLevelsNumber();
  void setFirstLevelBranchesNumber(uint branches);
  void setNextLevelBranchesDecrement(int decrement);
  void setQmultiplier(float qMultiplier);
  void setAlphaMultiplier(float alphaMultiplier);
  void setAlpha(float alpha);

  void setRootLength(uint rootLength);
  void setLengthMultiplier(float lengthMultiplier);

  void setStartSize(uint startSize);
  void setDSize(float dSize);
  void setSizeMultiplier(float sizeMultiplier);
  void setTreeColor(QColor treeColor);
  void setBackgroundColor(QColor backgroundColor);
  QColor getBackgroundColor();
  QString getLastGeneratedSvg();

private:
  TreeGeneratingRules *Rules;
  QString lastGeneratedSvgString;
  QString lastGeneratedSvgTempFile;
  std::vector <QString> toBeDeletedList;
  void createNewTempFile(QString fileName, bool exportToPng);
  void exportAsPng(QString outputFileName, int width, int height);
};

#endif // TREEGENERATORINTERFACE_H
