/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef TREEGENERATINGRULES_H
#define TREEGENERATINGRULES_H

#include "branchparameters.h"
#include <vector>
#include <QColor>

class TreeGeneratingRules
{
public:


  uint currentLevel = 0;

  float qMultiplier = 1.0;
  float alphaMultiplier = 0.75;
  float tMultiplier = 0.5;
  float sizeMultiplier = 0.6;
  std::vector<QColor> levelColors;
  QColor backgroundColor;
  uint branchesThisLvl= 9;
  int dBrnchesNextLvl = -2;

  uint branchLevels = 2;

  BranchParameters parameters;

  const uint rootLevel = 0;

  TreeGeneratingRules(QColor rootColor, QColor bgColor)
  {
    Initialize(rootColor, bgColor);
  }


  TreeGeneratingRules()
  {
    Initialize(Qt::black, Qt::white);
  }

  void Initialize(QColor rootColor, QColor bgColor)
  {
    parameters.startX = 512;
    parameters.startY = 768;
    parameters.startDirectionX = 0.0;
    parameters.startDirectionY = -1.0;

    parameters.T = 500;
    parameters.m = 0.001;
    parameters.q = 0.001;
    parameters.alpha = 1.4;
    parameters.size = 7;
    parameters.dSize = -0.015;
    parameters.dt = 1.0;
    parameters.fb = 0.001;
    parameters.color = rootColor;
    levelColors.push_back(rootColor);
    backgroundColor = bgColor;
  }

  bool WillBranchesUnderflow()
  {
    return (currentLevel != rootLevel) && (dBrnchesNextLvl < 0) && ((-dBrnchesNextLvl) >= branchesThisLvl);

  }

  bool NextLevel()
  {
    if (currentLevel < branchLevels && (!WillBranchesUnderflow()))
    {

      parameters.T = parameters.T * tMultiplier;
      parameters.alpha = parameters.alpha * alphaMultiplier;
      parameters.q = parameters.q * qMultiplier;
      
      if (currentLevel != rootLevel)
      {
        branchesThisLvl = branchesThisLvl + dBrnchesNextLvl;
      } 
      
      if (levelColors.size() > currentLevel)
      {
        parameters.color = levelColors[currentLevel];
      }

      currentLevel++;

      return true;
    }
    else
    {
      return false;
    }
  }



};

#endif // TREEGENERATINGRULES_H
