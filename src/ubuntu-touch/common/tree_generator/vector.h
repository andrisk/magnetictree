/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef VECTOR
#define VECTOR

class Vector3
{
public:
    float X;
    float Y;
    float Z;

    Vector3(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    }

    Vector3()
    {
        X = 0;
        Y = 0;
        Z = 0;
    }

    Vector3 operator+ (Vector3 other)
    {
        float newX = X + other.X;
        float newY = Y + other.Y;
        float newZ = Z + other.Z;

        return Vector3(newX, newY, newZ);
    }

    Vector3 operator* (float scalar)
    {
        float newX = X * scalar;
        float newY = Y * scalar;
        float newZ = Z * scalar;

        return Vector3(newX, newY, newZ);
    }

    Vector3 operator/ (float scalar)
    {
        return Vector3(X/scalar, Y/scalar, Z/scalar);
    }

    Vector3 operator- (Vector3 other)
    {
        float newX = X - other.X;
        float newY = Y - other.Y;
        float newZ = Z - other.Z;

        return Vector3(newX, newY, newZ);
    }

    float Dot(Vector3& other)
    {
        return X * other.X + Y * other.Y + Z * other.Z;
    }

    Vector3 Cross(Vector3 other)
    {
        float newX = Y*other.Z - Z*other.Y;
        float newY = Z*other.X - X*other.Z;
        float newZ = X*other.Y - Y*other.X;
        return Vector3(newX, newY, newZ);
    }

private:



};

#endif // VECTOR

