/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "branch.h"
#include <sstream>
#include <algorithm>

branch::branch(BranchParameters parameters, branch* paramParent)
{
  p = parameters;
  parent = paramParent;
}

branch::~branch()
{
  points.clear();
}

void branch::tracePath()
{
  float velocity[3] = {p.startDirectionX, p.startDirectionY, 0};
  electron *e = new electron(p.startX, p.startY,velocity, p.m, p.q, p.T, p.alpha);


  maxX = p.startX + p.size;
  minX = p.startX - p.size;
  maxY = p.startY + p.size;
  minY = p.startY - p.size;
  
  float currentSize = p.size;

  for (double t = 0; t < p.T; t+=p.dt)
  {
    e->step(p.fb, p.dt);
    points.push_back(*(new branchPoint(e->x, e->y, currentSize, e->v[0], e->v[1])));
    currentSize = currentSize+ p.dSize;
    if (currentSize < 1.0)
    {
      currentSize = 1.0;
    }
    maxX = std::max(e->x + currentSize, maxX);
    minX = std::min(e->x - currentSize, minX);
    maxY = std::max(e->y + currentSize, maxY);
    minY = std::min(e->y - currentSize, minY);
  }
}

std::string branch::getSvgPart()
{
  std::string branchSvg;
  std::ostringstream circleStream;
  for (uint index = 0; index < points.size(); index++)
  {
    circleStream << "<circle cx=\"" << points[index].x << "\" cy=\"" << points[index].y << "\" r=\"" << points[index].width << "\" stroke-width=\"0\" stroke=\"" << p.color.name().toStdString() << "\" fill=\"" << p.color.name().toStdString() << "\" />\n";
  }
  branchSvg.append(circleStream.str());
  return branchSvg;
}

uint branch::getBranchLevel()
{
  uint level = 0;
  branch* currentBranch = this;
  while (currentBranch->parent != NULL)
  {
    currentBranch = currentBranch->parent;
    level++;
  }
  return level;
}


void branch::ClearRenderData()
{
  points.clear();
}
