/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "electron.h"
#include "vector.h"
#include <math.h>

#ifdef USE_GLM

#include "glm.hpp"

glm::vec3 borisMethodStepGlm(glm::vec3 B_vec, glm::vec3 E_vec, float dt, float qdivm, glm::vec3 v_vec)
{
  glm::vec3 v_minus_vec;
  glm::vec3 v_prime_vec;
  glm::vec3 v_plus_vec;
  glm::vec3 t_vec;
  glm::vec3 s_vec;
  float t_mag2;

  t_vec = qdivm * B_vec * (0.5f * dt);

  t_mag2 = glm::dot(t_vec, t_vec);

  s_vec  = 2.0f * t_vec / (1.0f + t_mag2);

  v_minus_vec = v_vec + qdivm * E_vec * 0.5f * dt;

  glm::vec3 v_minus_cross_t_vec = glm::cross(v_minus_vec,t_vec);

  v_prime_vec = v_minus_vec + v_minus_cross_t_vec;

  glm::vec3 v_prime_cross_s_vec = glm::cross(v_prime_vec,s_vec);

  v_plus_vec = v_minus_vec + v_prime_cross_s_vec;

  v_vec = v_plus_vec + qdivm * E_vec*0.5f*dt;

  return v_vec;
}


void electron::stepGlm(float Fb, float dt)
{
  glm::vec3 B(0,0,Fb);
  glm::vec3 E(0,0,0);
  glm::vec3 new_vel = borisMethodStepGlm(B, E, dt, q/m, glm::vec3(v[0], v[1], v[2]));
  v[0] = new_vel[0];
  v[1] = new_vel[1];
  v[2] = new_vel[2];
  x += v[0];
  y += v[1];
  q = electron::newQ();
  t++;
}

void electron::step(float Fb, float dt)
{
    electron::stepGlm(Fb, dt);
}
#else

void electron::step(float Fb, float dt)
{
    electron::stepNoGlm(Fb, dt);
}

#endif


electron::electron(float pos_x, float pos_y, float velocity[3], float mass, float chargeQ, float lifespan, float alphaQ)
{
  x=pos_x;
  y=pos_y;
  for (int i=0; i<3; i++)
  {
    v[i] = velocity[i];
  }
  m = mass;
  q = chargeQ;
  t = 0;
  maxT = lifespan;
  alpha = alphaQ;
}

Vector3 borisMethodStepNoGlm(Vector3 B_vec, Vector3 E_vec, float dt, float qdivm, Vector3 v_vec)
{
    Vector3 v_minus_vec;
    Vector3 v_prime_vec;
    Vector3 v_plus_vec;
    Vector3 t_vec;
    Vector3 s_vec;
    float t_mag2;

    t_vec =  B_vec * qdivm * (0.5f * dt);

    t_mag2 = t_vec.Dot(t_vec);

    s_vec  = (t_vec * 2.0f) / (t_mag2 + 1.0f);

    v_minus_vec = v_vec + E_vec * qdivm * 0.5f * dt;

    Vector3 v_minus_cross_t_vec = v_minus_vec.Cross(t_vec);

    v_prime_vec = v_minus_vec + v_minus_cross_t_vec;

    Vector3 v_prime_cross_s_vec = v_prime_vec.Cross(s_vec);

    v_plus_vec = v_minus_vec + v_prime_cross_s_vec;

    v_vec = v_plus_vec + E_vec*qdivm*0.5f*dt;

    return v_vec;
}


void electron::stepNoGlm(float Fb, float dt)
{
  Vector3 B(0,0,Fb);
  Vector3 E(0,0,0);
  Vector3 new_vel = borisMethodStepNoGlm(B, E, dt, q/m, Vector3(v[0], v[1], v[2]));
  v[0] = new_vel.X;
  v[1] = new_vel.Y;
  v[2] = new_vel.Z;
  x += v[0];
  y += v[1];
  q = electron::newQ();
  t++;
}

float electron::newQ()
{
  return q>0 ? pow(maxT - t, -alpha) : -pow(maxT - t, -alpha);
}



