/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "magnetictree.h"
#include <algorithm>
#include <sstream>

magneticTree::magneticTree()
{

}

void magneticTree::Generate(TreeGeneratingRules rules, branch *parent)
{
  backgroundColor = rules.backgroundColor;
  if (parent == NULL)
  {
    branch* newBranch = new branch(rules.parameters, NULL);
    newBranch->tracePath();
    branches.push_back(*newBranch);
    Generate(rules, newBranch);
  }
  else
  {
    if (rules.NextLevel())
    {
      uint parentTracePointCount = parent->points.size();
      uint separationOfBranchesInPoints = parentTracePointCount / (rules.branchesThisLvl + 1);
      for (uint x=1; x<rules.branchesThisLvl+1; x++)
      {
        branchPoint currentStartPoint = parent->points[x*separationOfBranchesInPoints];
        rules.parameters.startX = currentStartPoint.x;
        rules.parameters.startY = currentStartPoint.y;
        rules.parameters.startDirectionX = currentStartPoint.vx;
        rules.parameters.startDirectionY = currentStartPoint.vy;
        rules.parameters.q = -rules.parameters.q;
        rules.parameters.size = currentStartPoint.width * rules.sizeMultiplier;
        branch* newBranch = new branch(rules.parameters, parent);
        newBranch->tracePath();
        branches.push_back(*newBranch);
        Generate(rules, newBranch);
      }
    }
  }
}

void magneticTree::ClearRenderData()
{
  for (uint x=0; x < branches.size(); x++)
  {
    branches[x].ClearRenderData();
  }
  branches.clear();
}

void magneticTree::TracePaths()
{
  if (branches.size() > 0)
  {
    maxX = branches[0].p.startX;
    minX = branches[0].p.startX;
    maxY = branches[0].p.startY;
    minY = branches[0].p.startY;
  }
  
  for (uint x=0; x<branches.size(); x++)
  {
    branches[x].tracePath();
    maxX = std::max(branches[x].maxX, maxX);
    minX = std::min(branches[x].minX, minX);
    maxY = std::max(branches[x].maxY, maxY);
    minY = std::min(branches[x].minY, minY);
  }
}

QString magneticTree::GenerateSvg()
{
  QString svgImage;
  svgImage.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
  svgImage.append("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\">");
  std::ostringstream backgroundRectStream;
  backgroundRectStream << "<rect x=\"" << minX << "\" y=\"" << minY << "\" width=\"" << maxX - minX << "\" height=\"" << maxY - minY << "\" style=\"fill:" << backgroundColor.name().toStdString() << "\" />";
  svgImage.append(QString::fromStdString(backgroundRectStream.str()));

  for (uint x=0; x<branches.size(); x++)
  {
    svgImage.append(branches[x].getSvgPart().c_str());
  }


  svgImage.append("</svg>");
  return svgImage;
}
