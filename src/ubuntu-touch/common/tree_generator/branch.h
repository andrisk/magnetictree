/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef BRANCH_H
#define BRANCH_H

#include <vector>
#include "branchpoint.h"
#include "electron.h"
#include "branchparameters.h"
#include <string>

//#include <QGraphicsScene>

class branch
{
public:
  branch(BranchParameters parameters, branch* paramParent);
  ~branch();

  void tracePath();
  void ClearRenderData();
  uint getBranchLevel();
  std::string getSvgPart();

  BranchParameters p;
  branch *parent;
  double parentTwhenBranched;
  std::vector<branchPoint> points;
  float maxX, minX, maxY, minY;

};

#endif // BRANCH_H
