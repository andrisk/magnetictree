/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019, 2021 Andrej Trnkoci <andrej.trnkoci@gmail.com>         *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU Affero General Public License as published   *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU Affero General Public License for more details.                        *
 *                                                                             *
 *  You should have received a copy of the GNU Affero General Public License   *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <QQmlApplicationEngine>
#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QQuickStyle>
#include "treegeneratorinterface.h"
#include <libintl.h>
#include <locale.h>

int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("MagneticTree");
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    qmlRegisterType<TreeGeneratorInterface>("MagneticTree.Interface", 1,0, "TreeGeneratorInterface");
    QQuickStyle::setStyle("Suru");
    QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
    app->setApplicationName("magnetictree.andrisk");
    QCoreApplication::setApplicationVersion(QStringLiteral(APP_VERSION));
    setlocale(LC_ALL, "");
    bindtextdomain("magnetictree.andrisk", "./share/locale");
    bind_textdomain_codeset("magnetictree.andrisk", "UTF-8");
    textdomain("magnetictree.andrisk");

    QQmlApplicationEngine engine;
    //engine.rootContext()->setContextProperty("availableStyles", QQuickStyle::availableStyles());
    engine.load(QUrl("qml/Main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app->exec();
}
