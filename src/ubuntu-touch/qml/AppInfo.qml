/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019, 2023 Andrej Trnkoci <andrej.trnkoci@gmail.com>         *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3  as UITK

Item {
    GridLayout
    {
        id: grid
        columnSpacing: 15
        rowSpacing: 5
        anchors.fill:parent
        anchors.margins: 5
        columns: 2
        Label
        {
            text: i18n.tr("Magnetic Tree")
            font.bold: true
            font.pointSize: 22
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter
        }

        Label
        {
            text: i18n.tr("Version") + " " + Qt.application.version
            Layout.rowSpan: 1
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter
        }

        UITK.LomiriShape {
            Layout.preferredHeight: units.gu(10)
            Layout.preferredWidth: units.gu(10)
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Layout.columnSpan: 2

            source:  Image {
                mipmap: true
                source: '../assets/logo.svg'

            }
        }


        Label
        {
            text: i18n.tr("License") + ":"
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }

        Label
        {
            text: "GPLv3"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://www.gnu.org/licenses/gpl-3.0.html")
            }
        }

        Label
        {
            text: i18n.tr("Get the source") + ":"
            Layout.alignment: Qt.AlignRight
        }

        Label
        {
            text: "gitlab"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/andrisk/magnetictree")
            }
        }

        Label
        {
            text: i18n.tr("Report issues") + ":"
            Layout.alignment: Qt.AlignRight
        }

        Label
        {
            text: "gitlab"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/andrisk/magnetictree/issues")
            }
        }

        Label
        {
            text: i18n.tr("App Development") + ":"
            Layout.alignment: Qt.AlignRight
        }
        Label
        {
            //Layout.columnSpan: 2
            text: "Andrej Trnkóci"
            font.underline: true
            //textFormat: Text.PlainText
            //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/andrisk")
            }
        }

        Label
        {
            text: i18n.tr("Translations")  + " (fr):"
            Layout.alignment: Qt.AlignRight
        }
        Label
        {
            text: "Anne Onyme 017"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/Anne17")
            }
        }

        Label
        {
            text: i18n.tr("Translations") + " (sk,cs):"
            Layout.alignment: Qt.AlignRight
        }
        Label
        {
            text: "Andrej Trnkóci"
            font.underline: true
            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/andrisk")
            }
        }
    }

}
