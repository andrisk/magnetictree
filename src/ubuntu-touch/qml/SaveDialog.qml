/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 - 2020, 2023 Andrej Trnkoci <andrej.trnkoci@gmail.com>  *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3
import QtQuick.Controls 2.2


Page {
    id: saveDialog
    //anchors.fill: parent
    property var activeTransfer
    property var treeGeneratorInterface

    signal saveDialogEnd()
    RadioButton
    {
        id: pngOption
        text: "png"
        height: font.pixelSize * 3
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
    RadioButton
    {
        id: svgOption
        text: "svg"
        height: font.pixelSize * 3
        anchors.bottom: pngOption.top
        anchors.horizontalCenter: parent.horizontalCenter
        checked: true
    }

    Label
    {
        id: formatSelectText
        text: i18n.tr("Image format") + ":"
        height: font.pixelSize * 2
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: svgOption.top
    }

    Rectangle {
        anchors.top: parent.top
        width: parent.width
        anchors.bottom: formatSelectText.top



        ContentItem {
            id: exportDataItem
        }

        ContentPeerPicker {
            id: peerPicker
            visible: saveDialog.visible
            handler: ContentHandler.Destination
            contentType: ContentType.Pictures

            onPeerSelected: {
                activeTransfer = peer.request();
                var items = [];

                var date = (new Date()).toISOString()
                var suggestedPath;
                var realFilePath;
                if (svgOption.checked)
                {
                    suggestedPath = "/home/phablet/.cache/magnetictree.andrisk/" + date + ".svg"
                    realFilePath = treeGenerator.saveImageFileAsTemporary(suggestedPath, false)
                }
                else
                {
                    suggestedPath = "/home/phablet/.cache/magnetictree.andrisk/" + date + ".png"
                    realFilePath = treeGenerator.saveImageFileAsTemporary(suggestedPath, true)
                }

                exportDataItem.url = realFilePath;
                items.push(exportDataItem);
                activeTransfer.items = items;
                activeTransfer.state = ContentTransfer.Charged;
            }

            onCancelPressed: {
                saveDialog.saveDialogEnd()
            }

        }
    }
}

