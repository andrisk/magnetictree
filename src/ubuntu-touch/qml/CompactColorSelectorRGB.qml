/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 - 2021 Andrej Trnkoci <andrej.trnkoci@gmail.com>        *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.2

Rectangle
{
    id: dialRoot
    signal colorChanged(var red, var green, var blue)
    color: Qt.rgba(dialRed.value,dialGreen.value,dialBlue.value,100)
    radius: 4
    border.color: "#000000"
    border.width: 1
    property real redValue;
    property real greenValue;
    property real blueValue

    property int dialRadius: Math.min(dialRoot.width / 3.2, dialRoot.height * 0.9)

    onWidthChanged: update()
    onHeightChanged: update()

    Rectangle
    {
        width: dialRadius
        height: dialRadius
        radius: dialRadius
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 5
        color: "#990000"

        Dial
        {
            id: dialRed
            padding: 5
            width: dialRadius
            height: dialRadius
            from: 0
            to: 1
            value: redValue
            onValueChanged: dialRoot.colorChanged(value, dialGreen.value, dialBlue.value)
        }
    }

    Rectangle
    {
        width: dialRadius
        height: dialRadius
        radius: dialRadius
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 5
        color: "#009900"

        Dial
        {
            id: dialGreen
            padding: 5
            width: dialRadius
            height: dialRadius
            from: 0
            to: 1
            value: greenValue
            onValueChanged: dialRoot.colorChanged(dialRed.value, value, dialBlue.value)
        }
    }


    Rectangle
    {
        width: dialRadius
        height: dialRadius
        radius: dialRadius
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.margins: 5
        color: "#000099"

        Dial
        {
            id: dialBlue
            padding: 5
            width: dialRadius
            height: dialRadius
            from: 0
            to: 1
            value: blueValue
            onValueChanged: dialRoot.colorChanged(dialRed.value, dialGreen.value, value)
        }
    }
}
