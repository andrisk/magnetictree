/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019, 2021, 2023 Andrej Trnkoci <andrej.trnkoci@gmail.com>   *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import Lomiri.Components 1.3 as UITK
//import Qt.labs.settings 1.0
import MagneticTree.Interface 1.0

ApplicationWindow {
    id: root
    objectName: 'mainView'
    visible: true
    width: 432
    height: 768
    //width: units.gu(45)
    //height: units.gu(75)


    TreeGeneratorInterface
    {
        id: treeGenerator
    }

    header: UITK.PageHeader
    {
        leadingActionBar {
          actions: [
              UITK.Action {
                    iconName: "stock_image"
                    text: i18n.tr("View")
                    onTriggered:
                    {
                        parametersImageRow.viewParameters = false
                        mainPageStack.push(parametersImageRow)

                    }
               },
               UITK.Action {
                    iconName: "filters"
                    text: i18n.tr("Parameters")
                    onTriggered:
                    {
                        parametersImageRow.viewParameters = true
                        mainPageStack.push(parametersImageRow)
                    }
               }
           ]
           numberOfSlots: 2
        }

        trailingActionBar {
          actions: [
              UITK.Action {
                    iconName: "info"
                    text: i18n.tr("Info")
                    onTriggered:
                    {
                        mainPageStack.push(infoPage)
                    }
                },
                UITK.Action {
                    iconName: "save"
                    text: i18n.tr("Save")
                    onTriggered:
                    {
                        saveSvg.treeGeneratorInterface = treeGenerator
                        mainPageStack.push(saveSvg)

                    }
                }
            ]
            numberOfSlots: 2
        }

    }

    UITK.PageStack{
        Component.onCompleted:
        {
            imgPage.color = treeGenerator.backgroundColor
            parametersImageRow.viewParameters = true
            push(parametersImageRow)
        }
        width: root.width
        height: root.height
        id: mainPageStack

        RowLayout {
            id: parametersImageRow
            anchors.fill: parent
            visible: false
            property bool viewParameters

            ParametersPage
            {
                visible: parent.viewParameters
                id: parametersPage
                Layout.preferredWidth: 320
                Layout.preferredHeight: 600
                Layout.minimumWidth: 100
                Layout.minimumHeight: 100
                Layout.fillHeight: true
                Layout.fillWidth: true
                onDrawButtonClicked:
                {
                    imgPage.color = treeGenerator.backgroundColor
                    imgPage.imageSource = "data:image/svg+xml;base64," + treeGenerator.generateSvgImage()
                    if (parent.width < parent.height)
                    {
                        parametersImageRow.viewParameters = false;
                    }
                    mainPageStack.push(parametersImageRow)
                }
                onClearButtonClicked: imgPage.imageSource = ""
                onBranchLevelsValueChanged: treeGenerator.branchLevels = levels
                onFirstLevelBranchesNumberChanged: treeGenerator.firstLevelBranchesNumber = branches
                onNextLevelBranchesDecrementChanged: treeGenerator.nextLevelBranchesDecrement = decrement
                onQMultiplierChanged: treeGenerator.nextLevelBranchesDecrement.qMultiplier = qMultiplier
                onAlphaMultiplierChanged: treeGenerator.alphaMultiplier = alphaMultiplier
                onAlphaChanged: treeGenerator.alpha = alpha
                onRootLengthChanged: treeGenerator.rootLength = rootLength
                onLengthMultiplierChanged: treeGenerator.lengthMultiplier = lengthMultiplier
                onStartSizeChanged: treeGenerator.startSize = startSize
                onDSizeChanged: treeGenerator.dSize = dSize
                onSizeMultiplierChanged: treeGenerator.sizeMultiplier = sizeMultiplier
                onTreeColorChanged: treeGenerator.treeColor = Qt.rgba(red, green, blue, 1.0)
                onBackgroundColorChanged:
                {
                    treeGenerator.backgroundColor = Qt.rgba(red, green, blue, 1.0)
                }
            }

            ImagePage
            {
                visible: !parent.viewParameters || parent.width > parent.height
                id: imgPage
                Layout.preferredWidth: 400
                Layout.preferredHeight: 500
                Layout.minimumHeight: 100
                Layout.minimumWidth: 100
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
        SaveDialog
        {
            visible: false
            anchors.fill: parent
            id: saveSvg
            onSaveDialogEnd: mainPageStack.push(parametersImageRow)
        }

        AppInfo
        {
            visible: false
            anchors.fill: parent
            id: infoPage
        }
    }
}
