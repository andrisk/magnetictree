/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 - 2021 Andrej Trnkoci <andrej.trnkoci@gmail.com>        *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Window 2.2

Item {
    id: parametersRoot
    signal drawButtonClicked()
    signal clearButtonClicked()
    signal branchLevelsValueChanged(var levels)
    signal firstLevelBranchesNumberChanged(var branches)
    signal nextLevelBranchesDecrementChanged(var decrement)
    signal qMultiplierChanged(var qMultiplier)
    signal alphaMultiplierChanged(var alphaMultiplier)
    signal lengthMultiplierChanged(var lengthMultiplier)
    signal rootLengthChanged(var rootLength)
    signal sizeMultiplierChanged(var sizeMultiplier)
    signal dSizeChanged(var dSize)
    signal startSizeChanged(var startSize)
    signal alphaChanged(var alpha)
    signal treeColorChanged(var red, var green, var blue)
    signal backgroundColorChanged(var red, var green, var blue)

    Flickable
    {
        id: flickable
        anchors.fill: parent
        anchors.margins: 5

        contentHeight: contentColumn.childrenRect.height
        //boundsBehavior: (contentHeight > parametersRoot.height) ? Flickable.DragAndOvershootBounds : Flickable.StopAtBounds
        boundsBehavior: Flickable.StopAtBounds
        /* Set the direction to workaround https://bugreports.qt-project.org/browse/QTBUG-31905
           otherwise the UI might end up in a situation where scrolling doesn't work */
        flickableDirection: Flickable.VerticalFlick

        ScrollBar.vertical: ScrollBar {
            width: 8
            policy: ScrollBar.AlwaysOn
        }

        Column{
            id: contentColumn
            anchors.left: parent.left
            anchors.right: parent.right
            spacing: 3

            property int fontSize:  Math.min(width / 17, height / 14) //4 * Screen.pixelDensity
            property int smallFontSize: fontSize * 0.9
            property int spinBoxHeight: fontSize * 2.5
            property int spinBoxWidth: Math.min(spinBoxHeight * 3.2, width * 0.8)
            property int captionTextWidth: width * 0.9

            property int rgbSelectorWidth: width * 0.7 //Math.min(rgbSelectorHeight * 4, width * 0.75)
            property int rgbSelectorHeight: rgbSelectorWidth / 3 //fontSize * 3
            property int sliderWidth: contentColumn.width * 0.8
            property int sliderHeight: spinBoxHeight
/*
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                id: testText
                font.pixelSize: contentColumn.smallFontSize
                text: Screen.pixelDensity
                maximumLineCount: 3
                wrapMode: Text.WordWrap
            }
*/
            //TODO create separate item for text elements

            Button
            {
                anchors.horizontalCenter: parent.horizontalCenter

                font.pixelSize: contentColumn.fontSize
                height: contentColumn.spinBoxHeight
                width: contentColumn.spinBoxWidth
                text: i18n.tr("Draw")
                onClicked:
                {
                    parametersRoot.drawButtonClicked()
                }
            }


            Column
            {
                width: parent.width;
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10

                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Branching levels") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                SpinBox
                {
                    width: contentColumn.spinBoxWidth
                    height: contentColumn.spinBoxHeight;
                    font.pixelSize: contentColumn.fontSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    from: 1
                    to: 5
                    onValueChanged: parametersRoot.branchLevelsValueChanged(value)
                    Component.onCompleted: { value = 2 }
                }
            }

            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("First level branches #") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                SpinBox
                {
                    width: contentColumn.spinBoxWidth
                    height: contentColumn.spinBoxHeight;
                    font.pixelSize: contentColumn.fontSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    onValueChanged: parametersRoot.firstLevelBranchesNumberChanged(value)
                    Component.onCompleted: { value = 9 }
                }
            }


            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Next level branches Δ") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                SpinBox
                {
                    width: contentColumn.spinBoxWidth
                    height: contentColumn.spinBoxHeight;
                    font.pixelSize: contentColumn.fontSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    from: -5
                    to:  5
                    onValueChanged: parametersRoot.nextLevelBranchesDecrementChanged(value)
                    Component.onCompleted: { value = -2 }
                }

            }

            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                //padding: 0

                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Trunk curvature") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                Slider
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    //alpha
                    from: 2.0
                    to: 0.5
                    stepSize: 0.001
                    onValueChanged: parametersRoot.alphaChanged(value)
                    Component.onCompleted: { value = 1.4 }
                    width: contentColumn.sliderWidth
                    height: contentColumn.sliderHeight
                    /*
                    handle: Rectangle
                    {
                        implicitWidth: 34
                        implicitHeight: 34
                    }*/
                }
            }


            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Curvature multiplier") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                Slider
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    //alphaMultiplier
                    from: 2
                    to: 0.5

                    stepSize: 0.005
                    onValueChanged: parametersRoot.alphaMultiplierChanged(value)
                    Component.onCompleted: { value = 0.75 }
                    width: contentColumn.sliderWidth
                    height: contentColumn.sliderHeight
                }
            }



            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Trunk length") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                SpinBox
                {
                    width: contentColumn.spinBoxWidth
                    height: contentColumn.spinBoxHeight;
                    font.pixelSize: contentColumn.fontSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    //T - start length
                    from: 1
                    to: 1000
                    stepSize: 5
                    onValueChanged: parametersRoot.rootLengthChanged(value)
                    Component.onCompleted: { value = 500 }
                }
            }


            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Length multiplier") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                Slider
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    //tMultiplier
                    from: 0.2
                    to: 2.0
                    stepSize: 0.001
                    onValueChanged: parametersRoot.lengthMultiplierChanged(value)
                    Component.onCompleted: { value = 0.5 }
                    width: contentColumn.sliderWidth
                    height: contentColumn.sliderHeight
                }
            }


            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Trunk start width") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                SpinBox
                {
                    width: contentColumn.spinBoxWidth
                    height: contentColumn.spinBoxHeight;
                    font.pixelSize: contentColumn.fontSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    from: 1
                    to: 100
                    stepSize: 1

                    onValueChanged: parametersRoot.startSizeChanged(value)
                    Component.onCompleted: { value = 7 }
                }
            }


            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Width multiplier") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                Slider
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    //sizeMultiplier
                    from: 0.15
                    to: 1.5
                    stepSize: 0.01
                    onValueChanged: parametersRoot.sizeMultiplierChanged(value)
                    Component.onCompleted: { value = 0.6 }
                    width: contentColumn.sliderWidth
                    height: contentColumn.sliderHeight
                }
            }


            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Gradual width change") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                Slider
                {
                    anchors.horizontalCenter: parent.horizontalCenter
                    value: -0.015;
                    from: -0.04
                    to: 0.04

                    stepSize: 0.0004
                    onValueChanged: parametersRoot.dSizeChanged(value)
                    Component.onCompleted: { value = -0.015 }
                    width: contentColumn.sliderWidth
                    height: contentColumn.sliderHeight
                }
            }

            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Tree color") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                CompactColorSelectorRGB
                {
                    id: treeColorSelector
                    width: contentColumn.rgbSelectorWidth
                    height: contentColumn.rgbSelectorHeight
                    anchors.horizontalCenter: parent.horizontalCenter
                    onColorChanged: parametersRoot.treeColorChanged(red, green, blue)
                    redValue: 0
                    greenValue: 0
                    blueValue: 0
                }
            }

            Column
            {
                anchors.horizontalCenter: parent.horizontalCenter
                padding: 10
                Label
                {
                    width: contentColumn.captionTextWidth
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Background color") + ":"
                    font.pixelSize: contentColumn.smallFontSize
                    maximumLineCount: 3
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter;
                }
                CompactColorSelectorRGB
                {
                    id: backgroundColorSelector
                    width: contentColumn.rgbSelectorWidth
                    height: contentColumn.rgbSelectorHeight
                    anchors.horizontalCenter: parent.horizontalCenter
                    onColorChanged: parametersRoot.backgroundColorChanged(red, green, blue)
                    redValue: 1
                    greenValue: 1
                    blueValue: 1
                }
            }


        }
    }
}

