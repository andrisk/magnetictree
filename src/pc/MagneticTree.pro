TEMPLATE = app
TARGET = MagneticTree
QT += qml quick quickcontrols2 svg
DEPENDPATH += .
DEFINES += APP_VERSION=\\\"0.1.1\\\"

DESTDIR = $$PWD/Build

INCLUDEPATH += ../../src/ubuntu-touch/common/tree_generator

HEADERS += \
    ../ubuntu-touch/common/tree_generator/branch.h \
    ../ubuntu-touch/common/tree_generator/branchparameters.h \
    ../ubuntu-touch/common/tree_generator/branchpoint.h \
    ../ubuntu-touch/common/tree_generator/electron.h \
    ../ubuntu-touch/common/tree_generator/magnetictree.h \
    ../ubuntu-touch/common/tree_generator/treegeneratingrules.h \
    ../ubuntu-touch/common/tree_generator/treegeneratorinterface.h \
    ../ubuntu-touch/common/tree_generator/vector.h


SOURCES += \
    main.cpp \
    ../ubuntu-touch/common/tree_generator/branch.cpp \
    ../ubuntu-touch/common/tree_generator/branchparameters.cpp \
    ../ubuntu-touch/common/tree_generator/branchpoint.cpp \
    ../ubuntu-touch/common/tree_generator/electron.cpp \
    ../ubuntu-touch/common/tree_generator/magnetictree.cpp \
    ../ubuntu-touch/common/tree_generator/treegeneratingrules.cpp \
    ../ubuntu-touch/common/tree_generator/treegeneratorinterface.cpp \

DISTFILES += \
    qml/ImagePage.qml \
    qml/mainView.qml \
    qml/ParametersPage.qml \
    qml/AppInfo.qml

RESOURCES += \
    qml.qrc



