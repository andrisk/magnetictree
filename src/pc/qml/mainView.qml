/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 - 2020 Andrej Trnkoci <andrej.trnkoci@gmail.com>        *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import MagneticTree.Interface 1.0
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    id: applicationWindow
    width: 860
    height: 640


    TreeGeneratorInterface
    {
        id: treeGenerator
    }

    FileDialog {
        id: fileDialog
        title: "Pleas select a target file."
        selectExisting: false
        folder: shortcuts.pictures
        //defaultSuffix: "svg"
        //nameFilters: ["Svg file (*.svg)"]
        onAccepted: {
            treeGenerator.saveImageFile(fileDialog.fileUrl);
        }
    }

    AppInfo {
        id: aboutWnd
        width: 300
        height: 400
    }

    toolBar: ToolBar {

        RowLayout {
            anchors.fill: parent

            ToolButton
            {
                text: "Draw"
                onClicked: rightColumn.imageSource = "data:image/svg+xml;base64," + treeGenerator.generateSvgImage()
            }
            /*ToolButton
            {
                text: "Clear"
                onClicked: rightColumn.imageSource = ""
            }*/
            ToolButton
            {
                text: "Save Image"
                onClicked: fileDialog.open()
            }

            ToolButton
            {
                text: "About"
                //anchors.right: parent.right

                onClicked:
                {
                    aboutWnd.open()
                }
            }
            Item { Layout.fillWidth: true }
        }
    }


    RowLayout
    {
        anchors.fill: parent
        ParametersPage
        {
            id: leftColumn
            Layout.preferredWidth: 320
            Layout.preferredHeight: 600
            Layout.minimumWidth: 320
            Layout.minimumHeight: 400
            Layout.fillHeight: true
            Layout.fillWidth: true
            onDrawButtonClicked: rightColumn.imageSource = "data:image/svg+xml;base64," + treeGenerator.generateSvgImage
            onClearButtonClicked: rightColumn.imageSource = ""
            onBranchLevelsValueChanged: treeGenerator.branchLevels = levels
            onFirstLevelBranchesNumberChanged: treeGenerator.firstLevelBranchesNumber = branches
            onNextLevelBranchesDecrementChanged: treeGenerator.nextLevelBranchesDecrement = decrement
            onQMultiplierChanged: treeGenerator.nextLevelBranchesDecrement.qMultiplier = qMultiplier
            onAlphaMultiplierChanged: treeGenerator.alphaMultiplier = alphaMultiplier
            onAlphaChanged: treeGenerator.alpha = alpha

            onRootLengthChanged: treeGenerator.rootLength = rootLength
            onLengthMultiplierChanged: treeGenerator.lengthMultiplier = lengthMultiplier

            onStartSizeChanged: treeGenerator.startSize = startSize
            onDSizeChanged: treeGenerator.dSize = dSize
            onSizeMultiplierChanged: treeGenerator.sizeMultiplier = sizeMultiplier

        }
        ImagePage
        {
            id: rightColumn
            Layout.preferredWidth: 800
            Layout.preferredHeight: 600
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
    }
}
