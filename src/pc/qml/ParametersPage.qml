/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019, 2020 Andrej Trnkoci <andrej.trnkoci@gmail.com>         *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

Item {
    id: parametersRoot
    signal drawButtonClicked()
    signal clearButtonClicked()
    signal branchLevelsValueChanged(var levels)
    signal firstLevelBranchesNumberChanged(var branches)
    signal nextLevelBranchesDecrementChanged(var decrement)
    signal qMultiplierChanged(var qMultiplier)
    signal alphaMultiplierChanged(var alphaMultiplier)
    signal lengthMultiplierChanged(var lengthMultiplier)
    signal rootLengthChanged(var rootLength)
    signal sizeMultiplierChanged(var sizeMultiplier)
    signal dSizeChanged(var dSize)
    signal startSizeChanged(var startSize)
    signal alphaChanged(var alpha)

    //GroupBox
    //{
       // width: parent.width
       // height: 100
        //border.color: "black"
        //border.width: 1
        //anchors.margins: 5
        Grid
        {
            anchors.fill:parent
            anchors.margins: 5
            spacing: 5
            columns: 2


            Label{text: "Branching levels:"}
            SpinBox
            {
                onValueChanged: parametersRoot.branchLevelsValueChanged(value)
                activeFocusOnPress: false
                Component.onCompleted: { value = 2 }
            }
            Label{text: "First level branches #:"}
            SpinBox
            {
                onValueChanged: parametersRoot.firstLevelBranchesNumberChanged(value)
                activeFocusOnPress: false
                Component.onCompleted: { value = 9 }
            }

            Label{text: "Next level branches delta:"}
            SpinBox
            {
                minimumValue: -5
                maximumValue:  5
                onValueChanged: parametersRoot.nextLevelBranchesDecrementChanged(value)
                activeFocusOnPress: false
                Component.onCompleted: { value = -2 }
            }

            Label{text: "Root curvature:"}
            Slider
            {
                //alpha
                maximumValue: 5.0
                minimumValue: 0.2
                stepSize: 0.001
                onValueChanged: parametersRoot.alphaChanged(value)
                Component.onCompleted: { value = 1.4 }
            }

            Label{text: "Curvature multiplier:"}
            Slider
            {
                //alphaMultiplier
                maximumValue: 2
                minimumValue: 0.5
                stepSize: 0.005
                onValueChanged: parametersRoot.alphaMultiplierChanged(value)
                Component.onCompleted: { value = 0.75 }
            }



            Label{text: "Root length:"}
            SpinBox
            {
                //T - start length
                maximumValue: 1000
                minimumValue: 1
                stepSize: 50
                activeFocusOnPress: false
                onValueChanged: parametersRoot.rootLengthChanged(value)
                Component.onCompleted: { value = 500 }
            }
            Label{text: "Length multiplier:"}
            Slider
            {
                //tMultiplier
                maximumValue: 2.0
                minimumValue: 0.2
                stepSize: 0.001
                onValueChanged: parametersRoot.lengthMultiplierChanged(value)
                Component.onCompleted: { value = 0.5 }
            }

            Label{text: "Root start width:"}
            SpinBox
            {
                minimumValue: 1
                maximumValue: 100
                stepSize: 1
                onValueChanged: parametersRoot.startSizeChanged(value)
                activeFocusOnPress: false
                Component.onCompleted: { value = 7 }
            }

            Label{text: "Width multiplier:"}
            Slider
            {
                //sizeMultiplier
                maximumValue: 2.0
                minimumValue: 0.1
                stepSize: 0.001
                onValueChanged: parametersRoot.sizeMultiplierChanged(value)
                Component.onCompleted: { value = 0.6 }
            }

            Label{text: "Gradual width change:"}
            Slider
            {
                maximumValue: 0.1
                minimumValue: -0.1
                stepSize: 0.001
                activeFocusOnPress: false
                onValueChanged: parametersRoot.dSizeChanged(value)
                Component.onCompleted: { value = -0.015 }
            }

        }
    //}
}

