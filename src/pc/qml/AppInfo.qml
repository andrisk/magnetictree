/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019 Andrej Trnkoci <andrej.trnkoci@gmail.com>               *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

Dialog {
    id: aboutWindow
    width: 300
    height: 400

    contentItem:GridLayout
    {
        id: grid
        columnSpacing: 15
        rowSpacing: 5
        anchors.fill:parent
        anchors.margins: 10
        columns: 2
        Label
        {
            text: "Magnetic Tree"
            font.bold: true
            font.pointSize: 22
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter
        }

        Label
        {
            text: "version " + Qt.application.version
            Layout.rowSpan: 1
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter
        }

        Image {
            Layout.preferredHeight: 100
            Layout.preferredWidth: 100
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Layout.columnSpan: 2
            mipmap: true

            source:  "qrc:/assets/logo.svg"

        }


        Label
        {
            text: "License:"
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
        }

        Label
        {
            text: "GPLv3"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://www.gnu.org/licenses/gpl-3.0.html")
            }
        }

        Label
        {
            text: "Get the source:"
            Layout.alignment: Qt.AlignRight
        }

        Label
        {
            text: "gitlab"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/andrisk/magnetictree")
            }
        }

        Label
        {
            text: "Report issues:"
            Layout.alignment: Qt.AlignRight
        }

        Label
        {
            text: "gitlab"
            font.underline: true

            MouseArea {
                anchors.fill: parent
                onClicked: Qt.openUrlExternally("https://gitlab.com/andrisk/magnetictree/issues")
            }
        }


        Label
        {
            Layout.columnSpan: 2
            text: "2018 - 2019 © Andrej Trnkóci"
            textFormat: Text.PlainText
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }
    }

}
