/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                             *
 *  Copyright (C) 2019,2020 Andrej Trnkoci <andrej.trnkoci@gmail.com>          *
 *                                                                             *
 *  This file is part of MagneticTree.                                         *
 *                                                                             *
 *  MagneticTree is free software: you can redistribute it and/or modify       *
 *  it under the terms of the GNU General Public License as published          *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  MagneticTree is distributed in the hope that it will be useful,            *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU General Public License for more details.                               *
 *                                                                             *
 *  You should have received a copy of the GNU General Public License          *
 *  along with MagneticTree.  If not, see <https://www.gnu.org/licenses/>.     *
 *                                                                             *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#include <QtQuickControls2>
#include <QQuickStyle>
#include <QQuickView>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "treegeneratorinterface.h"

int main(int argc,char *argv[])
{

  qmlRegisterType<TreeGeneratorInterface>("MagneticTree.Interface", 1,0, "TreeGeneratorInterface");

  QQuickStyle::setStyle("Material");
  QGuiApplication app(argc, (char**)argv);
  app.setApplicationName("MagneticTree");
  QCoreApplication::setApplicationVersion(APP_VERSION);
  QQmlApplicationEngine engine;
  //QStyleHints *style = app.styleHints();
  QPalette pal = app.palette();
  pal.setColor(QPalette::Base, Qt::red);
  pal.setBrush(QPalette::Text, Qt::black);
  pal.setBrush(QPalette::WindowText, Qt::black);
  pal.setBrush(QPalette::Window, Qt::white);
  app.setPalette(pal);
  engine.load(QUrl(QStringLiteral("qrc:/qml/mainView.qml")));
  if (engine.rootObjects().isEmpty())
  {
      return -1;
  }

  return app.exec();
}
