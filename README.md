# Magnetic Tree

A nice tree generator. The application generates tree image in svg format. 

The application can be downloaded for ubuntu touch smartphone operating system. 

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/magnetictree.andrisk)

Also there is a version for PC that have not been packaged yet and needs to be built from source.

NOTE: Because cmake apparently does not like to have files outside of its main tree, even code shared between pc and ubuntu-touch version is all under src/ubuntu-touch/common.

The software is inspired by an article [Magnetic Curves: Curvature-Controlled Aesthetic Curves Using Magnetic Fields](https://pdfs.semanticscholar.org/ccac/0394825b1a8ab9933f8ca3449e5b66a5a526.pdf) by Ling Xu and David Mould (2009).

## Mobile version for Ubuntu Touch

The version is under folders src/ubuntu-touch/.

### Building

Magnetic tree Ubuntu Touch mobile version is built using [clickable](https://clickable.bhdouglass.com/en/latest/).

To build the application, run the following command from src/ubuntu-touch/ folder:

    clickable           #for the phone
    clickable desktop   #for desktop
    
## PC version

The version for PC - Linux (tested) or Windows (not teste yet) is in /src/pc folder.

### Building PC version

The simplest way should be using QT Creator.
*  Open src/pc/MagneticTree.pro file
*  The project should open in QT Creator
*  Make sure that all QT libraries needed for build are installed

## Contributing

*  All contributions must be licensed under GNU GPLv3 license. 
*  In case of reuse of code from different repository please make sure that it is legal to publish the code under GPLv3.
*  In case you wish to contribute non-code content for which GPLv3 license might not be ideal (images, translation texts), it is possible to use Creative Commons BY-SA v4.0 license. 
*  Please make sure to fill all needed information in the file header. 

## License

Copyright (C) 2018-2021, 2023 Andrej Trnkóci

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](http://www.gnu.org/licenses/)




